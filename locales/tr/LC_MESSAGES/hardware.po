# Compendium of tr.
msgid ""
msgstr ""
"Project-Id-Version: compendium-tr\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-15 19:31-0300\n"
"PO-Revision-Date: 2018-05-15 19:53-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/hardware.rst:4
msgid "Configuring Receipt Printers"
msgstr "Makbuz Yazıcılar Yapılandırma"

#: ../../source/hardware.rst:6
msgid ""
"The following instructions are for specific receipt printers, but can "
"probably be used to help with setup of other brands as well."
msgstr ""
"Aşağıdaki talimatlar özel makbuz yazıcılar içindir, ama muhtemelen diğer "
"markaların kurulumu için de yardım amaçlı kullanılabilir."

#: ../../source/hardware.rst:12
msgid "For Epson TM-T88III (3) & TM-T88IV (4) Printers"
msgstr "Epson TM-T88III (3) & TM-T88IV (4) Yazıcıları için"

#: ../../source/hardware.rst:17
msgid "In the Print Driver"
msgstr "Yazdırma Sürücüsünde"

#: ../../source/hardware.rst:19
msgid ""
"For these instructions, we are using version 5,0,3,0 of the Epson TM-T88III "
"print driver; the EPSON TM-T88IV version is ReceiptE4. Register at the "
"`EpsonExpert Technical Resource Center website <https://www.epsonexpert.com/"
"login>`__ to gain access to the drivers; go to Technical Resources, then "
"choose the printer model from the Printers drop-down list."
msgstr ""
"Bu talimatlar için Epson TM-T88III yazıcı sürücüsü 5,0,3,0 sürümünü "
"kullanıyoruz; EPSON TM-T88IV sürümü ise ReceiptE4. Sürücülere erişebilmek "
"için `EpsonExpert Technical Resource Center website <https://www.epsonexpert."
"com/login>`__ web sayfasında Technical Resources'a giderek ardından "
"Yazıcılar açılır menüsünden yazıcı modelinizi seçerek kayıt olunuz."

#: ../../source/hardware.rst:26
msgid ""
"Click Start > Printers and Faxes > Right click the receipt printer > "
"Properties:"
msgstr ""
"Başlat > Yazıcılar ve Fakslar > Makbuz yazıcısı > Özellikler üzerine sağ "
"tıklayın:"

#: ../../source/hardware.rst:29
msgid "Advanced Tab, click Printing Defaults button"
msgstr "Gelişmiş Sekme, Yazdırma Varsayılanları butonuna tıklayın"

#: ../../source/hardware.rst:31 ../../source/hardware.rst:68
msgid "Layout Tab: Paper size: Roll Paper 80 x 297mm"
msgstr "Tasarım sekmesi: Kağıt boyutu: Rulo kağıt 80 x 297 mm"

#: ../../source/hardware.rst:33 ../../source/hardware.rst:70
msgid "TM-T88III: Layout Tab: Check Reduce Printing and Fit to Printable Width"
msgstr ""
"TM-T88III: Düzen Sekmesi: Baskının yazdırılabilir bir genişliğe "
"indirgendiğini kontrol edin"

#: ../../source/hardware.rst:36
msgid ""
"TM-T88IV: Check Reduced Size Print; Click OK on the popup window that "
"appears. Fit to Printable Width should be automatically selected."
msgstr ""
"TM-T88IV: İndirgenmiş baskı boyutunu kontrol edin; Ekrana gelen açılır "
"pencerede TAMAM'ı tıklayın. Yazdırılabilir Genişliğe Sığdır otomatik olarak "
"seçilmelidir."

#: ../../source/hardware.rst:39
msgid "OK your way out of there."
msgstr "Çıkmak için Tamam'ı seçin."

#: ../../source/hardware.rst:44 ../../source/hardware.rst:114
msgid "In Firefox"
msgstr "Firefox'ta"

#: ../../source/hardware.rst:46 ../../source/hardware.rst:116
msgid "Under File > Page Setup:"
msgstr "Dosya > Sayfa Düzeni altında:"

#: ../../source/hardware.rst:48 ../../source/hardware.rst:118
msgid "Shrink to fit page on Format & Options tab"
msgstr "Biçim & Seçenekler sekmesinde Sayfaya sığacak şekilde küçült"

#: ../../source/hardware.rst:50 ../../source/hardware.rst:120
msgid ""
"0,0,0,0 for Margins on Margins & Header/Footer Tab. This makes the receipts "
"use all available space on the paper roll."
msgstr ""
"Kenar boşlukları & Üst bilgi/Alt bilgi sekmesi için 0,0,0,0. Bu ayar, "
"makbuzların kağıt rulosu üzerindeki mevcut tüm boşluğu kullanmasını sağlar."

#: ../../source/hardware.rst:53 ../../source/hardware.rst:123
msgid ""
"Set all Headers/Footers to -blank-. This removes all of the gunk you might "
"normally find on a print from Firefox, such as the URL, number of pages, etc."
msgstr ""
"Tüm Üstbilgi/Altbilgi'leri -boş- olarak ayarlayın. Bu işlem normalde bir "
"Firefox baskısı üzerinde görebileceğiniz URL, sayfa numaraları, vs. gibi "
"öğeleri kaldıracaktır."

#: ../../source/hardware.rst:57 ../../source/hardware.rst:127
#: ../../source/hardware.rst:229
msgid "Click OK"
msgstr "Tamam'ı tıklayın"

#: ../../source/hardware.rst:59 ../../source/hardware.rst:129
msgid ""
"Set the default printer settings in Firefox so you don't see a \"Print\" "
"dialog:"
msgstr ""
"Firefox'ta varsayılan yazıcı ayarlarını ayarlayın, böylece \"Yazdır\" "
"diyaloğunu görmezsiniz:"

#: ../../source/hardware.rst:62 ../../source/hardware.rst:132
msgid "Go to File > Print"
msgstr "Dosya > Yazdır'a gidin"

#: ../../source/hardware.rst:64 ../../source/hardware.rst:134
msgid "Set the Printer to the receipt printer."
msgstr "Makbuz yazıcısı için yazıcı ayarlayın."

#: ../../source/hardware.rst:66
msgid "Click the Advanced (or Properties) button"
msgstr "Gelişmiş (veya Özellikler) butonuna tıklayın"

#: ../../source/hardware.rst:73
msgid ""
"TM-T88IV: Check Reduced Size Print; click OK on the popup window that "
"appears. Fit to Printable Width should be automatically selected."
msgstr ""
"TM-T88IV: İndirgenmiş baskı boyutunu kontrol edin, ekrana gelen açılır "
"pencereden TAMAM üzerine tıklayın. Yazdırılabilir Genişliğe Sığdır otomatik "
"olarak seçilmiş olmalıdır."

#: ../../source/hardware.rst:76
msgid "OK your way out, go ahead and print whatever page you are on."
msgstr ""
"Bitirince TAMAM üzerine tıklayın, devam edin ve bulunduğunuz sayfayı "
"yazdırın."

#: ../../source/hardware.rst:78 ../../source/hardware.rst:138
msgid ""
"Type about:config, in the address bar. Click \"I'll be careful, I promise!\" "
"on the warning message."
msgstr ""
"Adres çubuğuna about:config yazın. Uyarı mesajında \"Dikkatli olacağıma söz "
"veriyorum!\" üzerine tıklayın."

#: ../../source/hardware.rst:81 ../../source/hardware.rst:141
msgid "Type, print.always in Filter."
msgstr "Filter içine print.always yazın."

#: ../../source/hardware.rst:83
msgid "Look for print.always\\_print\\_silent."
msgstr "print.always\\_print\\_silent komut dosyasını arayın."

#: ../../source/hardware.rst:85 ../../source/hardware.rst:145
msgid "If the preference is there then set the value to true."
msgstr "Eğer tercih oradaysa, bu sefer değeri true olarak ayarlayın."

#: ../../source/hardware.rst:87 ../../source/hardware.rst:147
msgid ""
"If the preference is not there (and it shouldn't be in most browsers) you "
"have to add the preference."
msgstr ""
"Eğer tercih orada değilse (ve çoğu web tarayıcıda olmaması gerekir) tercihi "
"eklemeniz gerekir."

#: ../../source/hardware.rst:90 ../../source/hardware.rst:150
msgid "Right click the preference area and select New > Boolean"
msgstr "Tercih alanına sağ tuşla tıklayın ve Yeni > Boolean seçin"

#: ../../source/hardware.rst:92
msgid ""
"Type print.always\\_print\\_silent in the dialog box and set the value to "
"True. This sets the print settings in Firefox to always use the same "
"settings and print without showing a dialog box."
msgstr ""
"İletişim kutusuna Print.always\\_print\\_silent yazın ve değeri True olarak "
"ayarlayın. Bu işlem Firefox'ta yazdırma seçeneklerini her zaman aynı "
"ayarları kullanacak ve iletişim kutusunun gösterilmesine gerek kalmadan "
"yazdırılacak şekilde ayarlar."

#: ../../source/hardware.rst:96 ../../source/hardware.rst:156
msgid "**Warning**"
msgstr "**Uyarı**"

#: ../../source/hardware.rst:98 ../../source/hardware.rst:158
msgid ""
"Setting the print.always\\_print\\_silent setting in about:config DISABLES "
"the ability to choose a printer in Firefox."
msgstr ""
"about:config içindeki print.always\\_print\\_silent ayarını ayarlamak, "
"Firefox'ta bir yazıcı seçme olanağını devre dışı BIRAKIR."

#: ../../source/hardware.rst:104
msgid "For Epson TM-T88II (2) Printers"
msgstr "Epson TM-T88II (2) Yazıcılar için"

#: ../../source/hardware.rst:106
msgid ""
"Register at the `EpsonExpert Technical Resource Center website <https://www."
"epsonexpert.com/login>`__ to gain access to the drivers; go to Technical "
"Resources, then choose the printer model from the Printers drop-down list."
msgstr ""
"Sürücülere erişebilmek için `EpsonExpert Technical Resource Center website "
"<https://www.epsonexpert.com/login>`__ adresinden kayıt olun; Technical "
"Resources bölümüne gidin ve Yazıcılar (Printers) açılır menüsünden yazıcı "
"modelini seçin."

#: ../../source/hardware.rst:136
msgid "Print whatever page you are on."
msgstr "Geçerli sayfayı yazdırın."

#: ../../source/hardware.rst:143
msgid "Look for, print.always\\_print\\_silent."
msgstr "print.always\\_print\\_silent komut dosyasını arayın."

#: ../../source/hardware.rst:152
msgid ""
"Type, print.always\\_print\\_silent in the dialog box and set the value to "
"True. This sets the print settings in Firefox to always use the same "
"settings and print without showing a dialog box."
msgstr ""
"Diyalog kutusu içine print.always\\_print\\_silent yazın ve değeri True "
"olarak ayarlayın. Bu işlem bir iletişim kutusu göstermeden her zaman aynı "
"ayarları kullanmak ve yazdırmak için Firefox'ta yazdırma ayarlarını ayarlar."

#: ../../source/hardware.rst:164
msgid "For Star SP542 Printers"
msgstr "Star SP542 Yazıcılar İçin"

#: ../../source/hardware.rst:169
msgid "Installing the Printer"
msgstr "Yazıcı Kurulumu"

#: ../../source/hardware.rst:171
msgid ""
"While the following comments are based on the Star SP542 receipt printer, "
"they probably apply to all printers in the SP5xx series."
msgstr ""
"Aşağıdaki yorumlar Star SP542 makbuz yazıcısını esas alsa da, muhtemelen "
"SP5xx serisindeki tüm yazıcılar için geçerli olacaktır."

#: ../../source/hardware.rst:174
msgid ""
"The Star SP542 receipt printer works well with Koha and **Firefox on Windows "
"XP SP3**. This printer, with either the parallel or USB interface, is fairly "
"easy to install and configure. You will need the following executable file "
"which is available from numerous places on the Internet:"
msgstr ""
"Star SP542 makbuz yazıcısı Koha ve **Windows XP SP3 üzerinde Firefox** ile "
"iyi çalışır. Paralel veya USB arayüzü ile olsun bu yazıcıyı yüklemek ve "
"yapılandırmak oldukça kolaydır. Internet üzerinde çeşitli yerlerde "
"bulabileceğiniz aşağıdaki yürütülebilir dosyaya ihtiyacınız olacaktır:"

#: ../../source/hardware.rst:180
msgid "linemode\\_2k-xp\\_20030205.exe"
msgstr "linemode\\_2k-xp\\_20030205.exe"

#: ../../source/hardware.rst:182
msgid ""
"This executable actually does all of the installation; you will not need to "
"use the Microsoft Windows \"Add Printer\" program. We recommend that when "
"installing, the option for the software monitor not be selected; we have "
"experienced significant pauses and delays in printing with it. Instead, "
"simply choose to install the receipt printer without the monitor."
msgstr ""
"Bu yürütülebilir dosya aslında tüm yüklemeyi yapar; Microsoft Windows "
"\"Yazıcı Ekle\" programına kullanmanıza gerek kalmayacaktır. Yükleme "
"yapılırken software monitoring (yazılım izleme) seçeneğinin seçilmemesini "
"tavsiye ediyoruz; Bu seçenek ile çıktı alırken önemli duraklamalar ve "
"gecikmeler tecrübe ettik. Bunun yerine sadece monitoring seçeneği olmadan "
"makbuz yazıcısı yüklemeyi seçin."

#: ../../source/hardware.rst:189
msgid ""
"Additionally, the install program may not put the printer on the correct "
"port, especially if using the USB interface. This is easily corrected by "
"going to \"Start -> Printers and Faxes -> Properties for the SP542 printer -"
"> Ports\", then check the appropriate port."
msgstr ""
"Ayrıca, kurulum programı yazıcınızı doğru port üzerinde "
"konumlandırmayabilir, özellikle bir USB arayüzü kullanıyorsanız. Bu hata şu "
"adımları izleyerek kolaylıkla düzeltilebilir \"Başla > Yazıcılar ve Fakslar "
"> SP542 Yazıcı Özellikleri > Portlar\" ve buradan yazıcınız için doğru portu "
"seçin."

#: ../../source/hardware.rst:194
msgid ""
"A reboot may be required, even if not indicated by the installation software "
"or the operating system."
msgstr ""
"Kurulum yazılımı veya işletim sistemi tarafından belirtilmese bile bir "
"yeniden başlatma gerekli olabilir."

#: ../../source/hardware.rst:197
msgid ""
"**Windows 7** users should refer to this page: http://www.starmicronics.com/"
"supports/win7.aspx."
msgstr ""
"**Windows 7** kullanıcıları bu sayfaya başvurmalıdır: `http://www."
"starmicronics.com/supports/win7.aspx."

#: ../../source/hardware.rst:203
msgid "Configuring Firefox to Print to Receipt Printer"
msgstr "Firefox tarayıcısını Makbuz Yazıcısı için yapılandırma"

#: ../../source/hardware.rst:205
msgid "Open File > Page Setup"
msgstr "Dosya Aç > Sayfa Düzeni"

#: ../../source/hardware.rst:207
msgid "Make all the headers and footers blank"
msgstr "Tüm üst bilgileri ve alt bilgileri boş bırak"

#: ../../source/hardware.rst:209
msgid "Set the margins to 0 (zero)"
msgstr "Kenar boşlukları 0 (sıfır) olarak ayarla"

#: ../../source/hardware.rst:211
msgid "In the address bar of Firefox, type about:config"
msgstr "Firefox adres çubuğuna about:config yazın"

#: ../../source/hardware.rst:213
msgid "Search for print.always\\_print\\_silent and double click it"
msgstr ""
"print.always\\_print\\_silent için arama yapın ve üzerine çift tıklayın"

#: ../../source/hardware.rst:215
msgid "Change it from false to true"
msgstr "Bunu false opsiyonundan true opsiyonuna değiştirin"

#: ../../source/hardware.rst:217
msgid ""
"This lets you skip the Print pop up box that comes up, as well as skipping "
"the step where you have to click OK, automatically printing the right sized "
"slip."
msgstr ""
"Bu işlem, ekrana gelen Yazdır açılır menüsünü atlamanızı, aynı zamanda "
"TAMAM'ı tıklamanız gerektiren adımı da atlayarak otomatik olarak doğru "
"ölçekli işlem fişini yazdırmanızı sağlar."

#: ../../source/hardware.rst:221
msgid "If print.always\\_print\\_silent does not come up"
msgstr "eğer print.always\\_print\\_silent bulunamazsa"

#: ../../source/hardware.rst:223
msgid "Right click on a blank area of the preference window"
msgstr "Tercih penceresinde boş bir alanı sağ tıklayın"

#: ../../source/hardware.rst:225
msgid "Select new > Boolean"
msgstr "yeni > Boolean seçin"

#: ../../source/hardware.rst:227
msgid "Enter \"print.always\\_print\\_silent\" as the name (without quotes)"
msgstr ""
"İsim olarak \"print.always\\_print\\_silent\" girin (tırnak işaretleri "
"olmadan)"

#: ../../source/hardware.rst:231
msgid "Select true for the value"
msgstr "Değer için true opsiyonunu seçin"

#: ../../source/hardware.rst:233
msgid "You may also want to check what is listed for print.print\\_printer"
msgstr ""
"Aynı zamanda print.print\\_printer için neyin listelendiğini kontrol etmek "
"isteyebilirsiniz"

#: ../../source/hardware.rst:235
msgid ""
"You may have to choose Generic/Text Only (or whatever your receipt printer "
"might be named)"
msgstr ""
"Sosyal/Salt Metin seçmek zorunda kalabilirsiniz (ya da makbuz yazıcınızın "
"adı her ne olacaksa)"
